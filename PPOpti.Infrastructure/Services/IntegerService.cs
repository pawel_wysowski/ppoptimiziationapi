﻿using PPOpti.Infrastructure.DTO;
using PPOpti.Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Timers;

namespace PPOpti.Services.Services
{
    public class IntegerService : IIntegerService
    {
        public class FakeData
        {
            private int dataInt;
            private bool dataBool;
            private string dataString;
            public FakeData(int _dataInt, bool _dataBool, string _dataString)
            {
                dataInt = _dataInt;
                dataBool = _dataBool;
                dataString = _dataString;
            }
        }

        public List<int> integerList = new List<int>(100000000);
        public ResultDTO GetForeachTime()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            foreach (int i in integerList)
            {
                FakeData f = new FakeData(i, false, "dsadsadffffffffffffffffffffASdas"+i);
            }

            stopwatch.Stop();
            ResultDTO resultDTO = new ResultDTO();
            resultDTO.resultTime = stopwatch.Elapsed.TotalMilliseconds;
            return resultDTO;
        }

        public ResultDTO GetForTime()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < integerList.Count; i++)
            {
                FakeData f = new FakeData(i, false, "dsadsadffffffffffffffffffffASdas" + i);
            }

            stopwatch.Stop();
            ResultDTO resultDTO = new ResultDTO();
            resultDTO.resultTime = stopwatch.Elapsed.TotalMilliseconds;
            return resultDTO;
        }

        public ResultDTO GetWhileTime()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            int i = 0;
            while (i < integerList.Count)
            {
                FakeData f = new FakeData(i, false, "dsadsadffffffffffffffffffffASdas" + i);
                i++;
            }

            stopwatch.Stop();
            ResultDTO resultDTO = new ResultDTO();
            resultDTO.resultTime = stopwatch.Elapsed.TotalMilliseconds;
            return resultDTO;
        }
    }
}
