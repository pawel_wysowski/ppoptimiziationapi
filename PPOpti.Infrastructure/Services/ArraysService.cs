﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using PPOpti.Infrastructure.DTO;

namespace PPOpti.Services.Services
{
    public class ArraysService : IArraysService
    {

        private int[,] array = new int[1000, 1000];

        public ResultDTO GetNotOptimizedArrayIteration()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < 1000; i++)
            {
                for(int j = 0; j < 1000; j++)
                {
                    array[j, i] = j;
                }
            }
            stopwatch.Stop();
            ResultDTO resultDTO = new ResultDTO();
            resultDTO.resultTime = stopwatch.Elapsed.TotalMilliseconds;

            return resultDTO;
        }

        public ResultDTO GetOptimizedArrayIteration()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < 1000; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    array[i, j] = j;
                }
            }
            stopwatch.Stop();
            ResultDTO resultDTO = new ResultDTO();
            resultDTO.resultTime = stopwatch.Elapsed.TotalMilliseconds;

            return resultDTO;
        }
    }
}
