﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using PPOpti.Data;
using PPOpti.Infrastructure.DTO;
using System.Linq;
namespace PPOpti.Services.Services
{
    public class QueryService : IQueryService
    {
        private readonly StudentContext studentContext;
        public QueryService(StudentContext _studentContext)
        {
            studentContext = _studentContext;
        }
        public ResultDTO GetStudentNotOptimized()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();


            var allStudentsList = studentContext.Students.Select(students => students).ToList();
            var studentFromWadowice = allStudentsList.Find(s => s.City.Equals("Wadowice"));


            stopwatch.Stop();
            ResultDTO resultDTO = new ResultDTO();
            resultDTO.resultTime = stopwatch.Elapsed.TotalMilliseconds;
            return resultDTO;
        }

        public ResultDTO GetStudentOptimized()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();


            var studentFromWadowice = studentContext.Students.Select(student => student.City.Equals("Wadowice"));

            stopwatch.Stop();
            ResultDTO resultDTO = new ResultDTO();
            resultDTO.resultTime = stopwatch.Elapsed.TotalMilliseconds;
            return resultDTO;
        }
    }
}
