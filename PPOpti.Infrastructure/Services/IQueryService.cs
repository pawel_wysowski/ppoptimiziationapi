﻿using PPOpti.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace PPOpti.Services.Services
{
    public interface IQueryService
    {
        ResultDTO GetStudentNotOptimized();
        ResultDTO GetStudentOptimized();
    }
}
