﻿using PPOpti.Infrastructure.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace PPOpti.Infrastructure.Services
{
    public interface IIntegerService
    {
        ResultDTO GetForTime();
        ResultDTO GetForeachTime();
        ResultDTO GetWhileTime();
    }
}
