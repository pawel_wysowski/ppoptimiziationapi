﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using PPOpti.Infrastructure.DTO;

namespace PPOpti.Services.Services
{
    public class MultiplicationService : IMultiplicationService
    {
        private double[] array = new double[1000000];

        public MultiplicationService()
        {
            for(int i = 0; i < array.Length; i++)
            {
                array[i] = i;
            }
        }

        public ResultDTO GetDivision()
        {
            double scale_factor = 17;

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < array.Length ; i++)
                array[i] /= scale_factor;

            stopwatch.Stop();
            ResultDTO resultDTO = new ResultDTO();
            resultDTO.resultTime = stopwatch.Elapsed.TotalMilliseconds;

            return resultDTO;
        }

        public ResultDTO GetMultiplication()
        {
            double scale_factor = 1/17;

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            for (int i = 0; i < array.Length; i++)
                array[i] *= scale_factor;

            stopwatch.Stop();
            ResultDTO resultDTO = new ResultDTO();
            resultDTO.resultTime = stopwatch.Elapsed.TotalMilliseconds;

            return resultDTO;
        }
    }
}
