﻿using Microsoft.EntityFrameworkCore;
using PPOpti.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace PPOpti.Data
{
    public class StudentContext : DbContext
    {
        public StudentContext(DbContextOptions<StudentContext> options)
        : base(options)
        { }

        public DbSet<Student> Students { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Student>();
        }
    }
}
