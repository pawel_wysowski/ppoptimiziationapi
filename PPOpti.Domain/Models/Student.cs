﻿using System;

namespace PPOpti.Domain
{
    public class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PhoneNumber { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
    }
}
