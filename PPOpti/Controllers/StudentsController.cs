﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PPOpti.Infrastructure.DTO;
using PPOpti.Services.Services;

namespace PPOpti.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IQueryService _queryService;

        public StudentsController(IQueryService queryService)
        {
            _queryService = queryService;
        }

        [HttpGet("optimized")]
        public ActionResult<ResultDTO> GetOptimizedStudent()
        {
            ResultDTO resultDTO = new ResultDTO();
            resultDTO = _queryService.GetStudentOptimized();
            return Ok(resultDTO);
        }

        [HttpGet("notoptimized")]
        public ActionResult<ResultDTO> GetNotOptimizedStudent()
        {
            ResultDTO resultDTO = new ResultDTO();
            resultDTO = _queryService.GetStudentNotOptimized();
            return Ok(resultDTO);
        }
    }
}