﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PPOpti.Infrastructure.DTO;
using PPOpti.Services.Services;

namespace PPOpti.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArraysController : ControllerBase
    {
        private readonly IArraysService _arraysService;
        public ArraysController(IArraysService arraysService)
        {
            _arraysService = arraysService;
        }

        // GET api/values
        [HttpGet("notoptimized")]
        public ActionResult<ResultDTO> GetNotOptimizedArrayInterationTime()
        {
            ResultDTO resultDTO = new ResultDTO();
            resultDTO = _arraysService.GetNotOptimizedArrayIteration();
            return Ok(resultDTO);
        }

        // GET api/values
        [HttpGet("optimized")]
        public ActionResult<ResultDTO> GetOptimizedArrayInterationTime()
        {
            ResultDTO resultDTO = new ResultDTO();
            resultDTO = _arraysService.GetOptimizedArrayIteration();
            return Ok(resultDTO);
        }
    }
}