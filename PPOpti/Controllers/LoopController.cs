﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PPOpti.Infrastructure.DTO;
using PPOpti.Infrastructure.Services;

namespace PPOpti.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoopController : ControllerBase
    {
        private readonly IIntegerService _integerService;
        public LoopController(IIntegerService integerService)
        {
            _integerService = integerService;
        }

        // GET api/values
        [HttpGet("foreach")]
        public ActionResult<ResultDTO> GetForeachTime()
        {
            ResultDTO resultDTO = new ResultDTO();
            resultDTO = _integerService.GetForeachTime();
            return Ok(resultDTO);
        }

        [HttpGet("for")]
        public ActionResult<ResultDTO> GetForTime()
        {
            ResultDTO resultDTO = new ResultDTO();
            resultDTO = _integerService.GetForTime();
            return Ok(resultDTO);
        }

        [HttpGet("while")]
        public ActionResult<ResultDTO> GetWhileTime()
        {
            ResultDTO resultDTO = new ResultDTO();
            resultDTO = _integerService.GetWhileTime();
            return Ok(resultDTO);
        }
    }
}
