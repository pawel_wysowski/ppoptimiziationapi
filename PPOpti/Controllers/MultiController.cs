﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PPOpti.Infrastructure.DTO;
using PPOpti.Services.Services;

namespace PPOpti.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MultiController : ControllerBase
    {
        private readonly IMultiplicationService _multiService;
        public MultiController(IMultiplicationService multiService)
        {
            _multiService = multiService;
        }

        // GET api/values
        [HttpGet("div")]
        public ActionResult<ResultDTO> GetDivisionTime()
        {
            ResultDTO resultDTO = new ResultDTO();
            resultDTO = _multiService.GetDivision();
            return Ok(resultDTO);
        }

        // GET api/values
        [HttpGet("multi")]
        public ActionResult<ResultDTO> GetMultiTime()
        {
            ResultDTO resultDTO = new ResultDTO();
            resultDTO = _multiService.GetMultiplication();
            return Ok(resultDTO);
        }
    }
}